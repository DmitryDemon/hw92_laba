import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";


import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import ChatList from "./containers/ChatList/ChatList";

const ProtectedRoute = ({isAllowed, ...props}) => {
  return isAllowed ? <Route {...props} /> : <Redirect to="/login" />
};

const Routes = (props) => {
    return (
    <Switch>
      <ProtectedRoute
        isAllowed={(props.user && props.user.role === 'admin') || (props.user && props.user.role === 'user')}
        path="/"
        exact
        render={() => <ChatList {...props} />}
      />
      <Route path="/register" exact component={Register}/>
      <Route path="/login" exact component={Login}/>
    </Switch>
  );
};

export default Routes;