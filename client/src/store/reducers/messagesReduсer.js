const initialState = {
  msg: [],
  users: [],
  // id: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'LATEST_MESSAGES':
      return {...state, msg: action.action.messages.reverse()};
    case 'ACTIVE_USERS':
      return {...state, users: action.action.usernames};
    case 'NEW_MESSAGE':
      return {...state, msg: [...state.msg].concat(action.action.message)};
    case 'DELETE_MESSAGE':
      return {...state, msg: [...state.msg].filter(m => m._id !== action._id)};
    default:
      return state;
  }
};

export default reducer;