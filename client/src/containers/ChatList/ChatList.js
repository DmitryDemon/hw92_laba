import React from 'react';
import './ChatList.css';

const ChatList = props => {
    return (
      <div className='wrapper' >
        <div className='online_user'>
          <h5 className='user_title'>Online users</h5>
          {props.msg.users.map((author, idx) => {
            return <p key={idx}>{author}</p>
          })}
        </div>
        <div className='chat_room'>
          <h5 className='chat_title'>Chat room</h5>
          <div className='chat_msg' ref={props.ref}>
            {props.msg.msg.map((message, idx) => (
              <p key={idx}>
                <b>{message.username}: </b>
                {message.text}
                  {props.user.role === 'admin' ? <button onClick={()=>props.remove(message._id)}>Delete</button>:null}
              </p>
            ))}
          </div>
        </div>
        <div className='send'>
          <form onSubmit={props.sendMessage}>
            <input
              className='input_msg'
              type="text"
              name="messageText"
              value={props.state.messageText}
              placeholder="Enter message"
              onChange={props.inputChangeHandler}
            />
            <input className='input_btn' type="submit" value="Send"/>
          </form>
        </div>
      </div>
    );
};

export default ChatList;