import React, {Component, Fragment} from 'react';
import {Container} from "reactstrap";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {NotificationContainer} from "react-notifications";

import {logoutUser} from "./store/actions/usersActions";

import Toolbar from "./components/UI/Toolbar/Toolbar";
import Routes from "./Routes";
import {chatAction, removeMessage} from "./store/actions/messagesActions";

class App extends Component {

    state = {
        messageText: '',
    };

    connector = () => {
        this.websocket = new WebSocket(`ws://localhost:8000/chat/chat?token=${this.props.user.token}`);

    this.websocket.onmessage = event => {
        const decodedMessage = JSON.parse(event.data);
        this.props.chatAction(decodedMessage);
    };

    this.websocket.onclose = () => {
       clearInterval(this.interval);
       this.interval = setInterval(() => {
          this.connector();
       }, 5000);
    }

};
    componentDidMount() {
        if (!this.props.user && this.websocket) this.websocket.close();
        if (this.props.user) {
            this.setState({username: this.props.user.username}, this.connector);
        }
    };

    componentDidUpdate(prevProps) {
        if (!this.props.user && this.websocket) this.websocket.close();
        if (!prevProps.user && this.props.user) {
            this.setState({username: this.props.user.username}, this.connector);
        }
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    sendMessage = event => {
        event.preventDefault();

        this.websocket.send(JSON.stringify({
            type: 'CREATE_MESSAGE',
            text: this.state.messageText
        }));
    };

    removeMessage = id => {
      this.websocket.send(JSON.stringify({
        type: 'REMOVE_MESSAGE',
        id
      }));
    };

  render() {
      console.log(this.props.msg);
      return (
        <Fragment>
          <NotificationContainer/>
          <header>
            <Toolbar
                user={this.props.user}
                logout={this.props.logoutUser}
            />
          </header>
          <Container style={{marginTop: '20px'}}>
            <Routes
                state={this.state}
                inputChangeHandler={this.inputChangeHandler}
                sendMessage={this.sendMessage}
                msg={this.props.msg}
                user={this.props.user}
                remove={this.props.removeMessage}
            />

          </Container>
        </Fragment>
    );
  }
}

const mapStateToProps = state => ({
    user: state.users.user,
    msg: state.msg,
});

const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser()),
    chatAction: action => dispatch(chatAction(action)),
    removeMessage: id => dispatch(removeMessage(id)),

});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));