const express = require('express');
const app = express();
const cors = require('cors');
const expressWs = require('express-ws')(app);
const mongoose = require('mongoose');
const config = require('./config');

const users = require('./app/users');
const chat = require('./app/chat');

const port = 8000;
app.use(cors());
app.use(express.json());



mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
    app.use('/users', users);
    app.use('/chat', chat);
    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });
});