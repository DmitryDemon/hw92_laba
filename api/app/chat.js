const express = require ('express');
const nanoid = require('nanoid');
const User = require('../models/User');
const Message = require('../models/Message');


const router = express.Router();

const activeConnections = {};

router.ws('/chat', async (ws, req) => {

    const token = req.query.token;

    if (!token) {
        return ws.close();
    }

    const user = await User.findOne({token});

    if (!user) {
        return ws.close();
    }

    const id = nanoid();
    activeConnections[id] = {user, ws};

    const usernames = Object.keys(activeConnections).map(connId => {
        const conn = activeConnections[connId];

        return conn.user.username;
    });

    Object.keys(activeConnections).forEach((connId) => {
        const conn = activeConnections[connId].ws;
        conn.send(JSON.stringify({
            type: 'ACTIVE_USERS',
            usernames
        }));
    });


  ws.send(JSON.stringify({
    type: 'LATEST_MESSAGES',
    messages: await Message.find().limit(30).sort({_id:-1})
  }));




    let username = user.username;

    ws.on('message', async msg => {
        let decodedMessage;
        try {
            decodedMessage = JSON.parse(msg);
        } catch (e) {
            return console.log('Not a valid message!');
        }

        switch (decodedMessage.type) {
            case 'CREATE_MESSAGE':
                try {
                    message = new Message({
                        username,
                        text: decodedMessage.text
                    });
                    await message.save();
                    Object.keys(activeConnections).forEach( connId => {
                        const conn = activeConnections[connId].ws;
                        conn.send(JSON.stringify({
                            type: 'NEW_MESSAGE',
                            message
                        }));
                    });
                }catch (e) {
                    throw new Error(`${e},'Ошибка при попытке сохранить сообщение`);
                }
                break;
            case 'REMOVE_MESSAGE':
                try {
                    await Message.findByIdAndRemove(decodedMessage.id);
                    Object.keys(activeConnections).forEach( connId => {
                        const conn = activeConnections[connId].ws;
                        conn.send(JSON.stringify({
                            type: 'DELETE_MESSAGE',
                            id: decodedMessage.id
                        }));
                    });
                }catch (e) {
                    throw new Error(`${e},'Ошибка при попытке удалить сообщение`);
                }


            default:
                console.log('Not valid message type,', decodedMessage.type);
        }
     });

    ws.on('close', msg => {
        delete activeConnections[id];
        const onlibeUsers = Object.keys(activeConnections).map(connId => {
            const conn = activeConnections[connId];

            return conn.user.username;
        });
        Object.keys(activeConnections).forEach((connId) => {
            const conn = activeConnections[connId].ws;
            conn.send(JSON.stringify({
                type: 'ACTIVE_USERS',
                usernames: onlibeUsers
            }));
        });

    });

});

module.exports = router;